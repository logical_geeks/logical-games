USE online_games;

IF OBJECT_ID ('dbo.battleMembersXref') IS NOT NULL DROP TABLE dbo.battleMembersXref; 

CREATE TABLE dbo.battleMembersXref (
	battleId  INT       NOT NULL, 
	playerId  SMALLINT  NOT NULL, 
	result    SMALLINT   NOT NULL, 
  
	CONSTRAINT PK_battleMembersXref PRIMARY KEY (
		battleId, 
		playerId
	)
);
