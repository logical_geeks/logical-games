USE online_games;

 IF OBJECT_ID ('dbo.privateChat') IS NOT NULL DROP TABLE dbo.privateChat; 

CREATE TABLE dbo.privateChat (
	privateChatId   INT       NOT NULL  IDENTITY, 
	firstPlayerId   SMALLINT  NOT NULL, 
	secondPlayerId  SMALLINT  NOT NULL, 
  
	CONSTRAINT PK_privateChat_privateChatId PRIMARY KEY (
		privateChatId
	)
);
