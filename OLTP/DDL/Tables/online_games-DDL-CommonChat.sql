USE online_games;

IF OBJECT_ID ('dbo.commonChat') IS NOT NULL DROP TABLE dbo.commonChat; 

CREATE TABLE dbo.commonChat (
	commonChatId    TINYINT  NOT NULL  IDENTITY, 
	gameTemplateId  TINYINT  NOT NULL, 
  
	CONSTRAINT PK_commonChat_commonChatId PRIMARY KEY (
		commonChatId
	)
);
