 USE online_games;

IF OBJECT_ID ('dbo.WrkBattleMove') IS NOT NULL DROP TABLE dbo.WrkBattleMove; 
 
CREATE TABLE dbo.WrkBattleMove (
	battleMoveId     TINYINT       NOT NULL  IDENTITY, 
	moveDescription  VARCHAR(100)  NOT NULL, 
  
	CONSTRAINT PK_wrkBattleMove_battleMoveId PRIMARY KEY (
		battleMoveId
	)
);
