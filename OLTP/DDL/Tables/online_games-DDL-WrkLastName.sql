USE online_games;

IF OBJECT_ID ('dbo.WrkLastName') IS NOT NULL DROP TABLE dbo.WrkLastName; 
 
CREATE TABLE dbo.WrkLastName (
	lastNameId  BIGINT        NOT NULL  IDENTITY, 
	lastName    VARCHAR(100)  NOT NULL, 
  
	CONSTRAINT PK_wrkLastName_lastNameId PRIMARY KEY (
		lastNameId
	)
);
