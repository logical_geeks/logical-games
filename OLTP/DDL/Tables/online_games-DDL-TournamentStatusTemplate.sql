USE online_games;

IF OBJECT_ID ('dbo.tournamentStatusTemplate') IS NOT NULL DROP TABLE dbo.tournamentStatusTemplate; 

CREATE TABLE dbo.tournamentStatusTemplate (
	tournamentStatusTemplateId    TINYINT      NOT NULL  IDENTITY, 
	tournamentStatusTemplateName  VARCHAR(20)  NOT NULL, 
  
	CONSTRAINT PK_tournamentStatusTemplate_tournamentStatusTemplateId PRIMARY KEY (
		tournamentStatusTemplateId
	)
);
