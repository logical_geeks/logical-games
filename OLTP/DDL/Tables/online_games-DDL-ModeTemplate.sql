USE online_games;

IF OBJECT_ID ('dbo.modeTemplate') IS NOT NULL DROP TABLE dbo.modeTemplate; 

CREATE TABLE dbo.modeTemplate (
	modeTemplateId    TINYINT      NOT NULL  IDENTITY, 
	modeTemplateName  VARCHAR(20)  NOT NULL, 
	gameTemplateId    TINYINT      NOT NULL, 
  
	CONSTRAINT PK_modeTemplate_modeTemplateId PRIMARY KEY (
		modeTemplateId
	)
);
