USE online_games;

IF OBJECT_ID ('dbo.chatMessage') IS NOT NULL DROP TABLE dbo.chatMessage; 

CREATE TABLE dbo.chatMessage (
	messageId         BIGINT        NOT NULL  IDENTITY, 
	messageTimeStamp  DATETIME      NOT NULL  DEFAULT CURRENT_TIMESTAMP, 
	message           VARCHAR(300)  NOT NULL, 
	chatTypeId        TINYINT       NOT NULL, 
	playerId          SMALLINT      NOT NULL, 
  
	CONSTRAINT PK_chatMessage_messageId PRIMARY KEY (
		messageId
	)
);
