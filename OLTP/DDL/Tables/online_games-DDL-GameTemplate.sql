USE online_games;

IF OBJECT_ID ('dbo.gameTemplate') IS NOT NULL DROP TABLE dbo.gameTemplate; 

CREATE TABLE dbo.gameTemplate (
	gameTemplateId    TINYINT      NOT NULL  IDENTITY, 
	gameTemplateName  VARCHAR(20)  NOT NULL, 
  
	CONSTRAINT PK_gameTemplate_gameTemplateId PRIMARY KEY (
		gameTemplateId
	)
);
