USE online_games;

IF OBJECT_ID ('dbo.tournamentMembersXref') IS NOT NULL DROP TABLE dbo.tournamentMembersXref; 


CREATE TABLE dbo.tournamentMembersXref (
	tournamentId  SMALLINT  NOT NULL, 
	playerId      SMALLINT  NOT NULL, 
  
	CONSTRAINT PK_tournamentMembersXref_tournamentId_playerId PRIMARY KEY (
		tournamentId, 
		playerId
	)
);
