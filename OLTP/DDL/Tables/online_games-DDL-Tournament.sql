USE online_games;

IF OBJECT_ID ('dbo.tournament') IS NOT NULL DROP TABLE dbo.tournament; 

CREATE TABLE dbo.tournament (
	tournamentId                SMALLINT     NOT NULL  IDENTITY, 
	tournamentName              VARCHAR(50)  NOT NULL, 
	startDate                   DATETIME     NOT NULL, 
	minMemberCount              SMALLINT, 
	modeTemplateId              TINYINT      NOT NULL, 
	tournamentStatusTemplateId  TINYINT      NOT NULL, 
  
	CONSTRAINT PK_tournament_tournamentId PRIMARY KEY (
		tournamentId
	)
);
