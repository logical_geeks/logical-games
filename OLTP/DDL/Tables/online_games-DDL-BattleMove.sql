USE online_games;

IF OBJECT_ID ('dbo.battleMove') IS NOT NULL DROP TABLE dbo.battleMove; 

CREATE TABLE dbo.battleMove (
	moveId           BIGINT        NOT NULL  IDENTITY, 
	moveTimeStamp    DATETIME      NOT NULL, 
	moveDescription  VARCHAR(200)  NOT NULL, 
	battleId         INT           NOT NULL, 
	playerId         SMALLINT      NOT NULL, 
  
	CONSTRAINT PK_battleMove_moveId PRIMARY KEY (
		moveId
	)
);
