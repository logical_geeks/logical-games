USE online_games;

IF OBJECT_ID ('dbo.WrkFirstName') IS NOT NULL DROP TABLE dbo.WrkFirstName; 
 
CREATE TABLE dbo.WrkFirstName (
	firstNameId  BIGINT        NOT NULL  IDENTITY, 
	firstName    VARCHAR(100)  NOT NULL, 
  
	CONSTRAINT PK_wrkFirstName_firstNameId PRIMARY KEY (
		firstNameId
	)
);
