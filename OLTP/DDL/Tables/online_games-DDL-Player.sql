USE online_games;

IF OBJECT_ID ('dbo.player') IS NOT NULL DROP TABLE dbo.player;


CREATE TABLE dbo.player (
	playerId          SMALLINT      NOT NULL  identity, 
	userName          VARCHAR(100)  NOT NULL, 
	userPassword      VARCHAR(100)  NOT NULL, 
	birthDate         DATE, 
	networkLink       VARCHAR(500), 
	city              VARCHAR(100),
	country           VARCHAR(100),
	about             VARCHAR(1000), 
	registrationDate  DATETIME      NOT NULL  DEFAULT CURRENT_TIMESTAMP, 
  
	CONSTRAINT PK_player_playerId PRIMARY KEY (
		playerId
	)
);
