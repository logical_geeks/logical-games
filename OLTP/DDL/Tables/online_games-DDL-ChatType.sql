USE online_games;

IF OBJECT_ID ('dbo.chatType') IS NOT NULL DROP TABLE dbo.chatType; 

CREATE TABLE dbo.chatType (
	chatTypeId    TINYINT      NOT NULL  IDENTITY, 
	chatTypeName  VARCHAR(10)  NOT NULL, 
  
	CONSTRAINT PK_chatType_chatTypeId PRIMARY KEY (
		chatTypeId
	)
);
