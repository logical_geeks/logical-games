USE online_games;

IF OBJECT_ID ('dbo.battle') IS NOT NULL DROP TABLE dbo.battle; 

CREATE TABLE dbo.battle (
	battleId        INT       NOT NULL  IDENTITY, 
	startDate       DATETIME  NOT NULL, 
	modeTemplateId  TINYINT   NOT NULL, 
	tournamentId    SMALLINT, 
  
	CONSTRAINT PK_battle_battleId PRIMARY KEY (
		battleId
	)
);
