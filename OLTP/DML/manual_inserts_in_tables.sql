USE online_games;

GO

INSERT INTO dbo.gameTemplate (gameTemplateName)
VALUES  ('Tic tac toe');

GO

INSERT INTO dbo.modeTemplate(modeTemplateName, gameTemplateId)
VALUES ('3x3'   , 1)
      ,('19x19' , 1)
      ,('Swap2' , 1);

GO

INSERT INTO dbo.commonChat (gameTemplateId)
SELECT gameTemplateId 
FROM dbo.gameTemplate;

GO

INSERT INTO dbo.chattype([chatTypeName])
VALUES ('common')
      ,('private');

GO

INSERT INTO dbo.tournamentStatusTemplate(tournamentStatusTemplateName)
VALUES ('Win')
      ,('Loss')
      ,('Draw');

GO

