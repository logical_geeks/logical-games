USE online_games;

GO

IF OBJECT_ID ('dbo.usp_insert_tournamentMembersXref') IS NOT NULL   
    DROP PROCEDURE dbo.usp_insert_tournamentMembersXref;  
GO  

CREATE PROCEDURE dbo.usp_insert_tournamentMembersXref
 
AS   
BEGIN

INSERT INTO dbo.tournamentMembersXref (tournamentId, playerId)
SELECT DISTINCT b.tournamentid, m.playerId
FROM dbo.battle AS B
INNER JOIN battleMembersXref AS M ON B.battleid = M.battleId
WHERE tournamentid IS NOT NULL
ORDER BY b.tournamentid

END