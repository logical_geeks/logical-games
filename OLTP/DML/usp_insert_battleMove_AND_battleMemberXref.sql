USE online_games;
GO

IF OBJECT_ID ( N'dbo.usp_insert_battleMove_AND_battleMemberXref', N'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.usp_insert_battleMove_AND_battleMemberXref;  
GO  

CREATE PROCEDURE dbo.usp_insert_battleMove_AND_battleMemberXref

AS  

IF OBJECT_ID(N'tempdb..#tmpPlayerId', N'U') IS NOT NULL
DROP TABLE #tmpPlayerId;  


CREATE TABLE #tmpPlayerId
(
PlayerSurrId      INT IDENTITY,
PlayerOriginalid  INT
);


INSERT INTO #tmpPlayerId (PlayerOriginalid)
SELECT playerid
FROM dbo.player;

--Temporary table to create surrogate id for battles

IF OBJECT_ID(N'tempdb..#tmpBattleId', N'U') IS NOT NULL
DROP TABLE #tmpBattleId;  


CREATE TABLE #tmpBattleId
(
BattleSurrId      INT IDENTITY,
BattleOriginalId  INT
);


INSERT INTO #tmpBattleId (BattleOriginalId)
SELECT battleId
FROM dbo.battle;

--Temporary table to insert generated rows before insert in the target table

IF OBJECT_ID(N'tempdb..#tmpBattlePlayer', N'U') IS NOT NULL
DROP TABLE #tmpBattlePlayer;  


CREATE TABLE #tmpBattlePlayer
(
battleId     INT,
PlayerId     INT,
RandomGameId INT,
PlayerBitId  INT,
);


DECLARE @counter       INT,
        @cntPlayer     INT,
        @cntBattle     INT,
        @CntAllGames   INT,
        @RandomPlayer1 INT,
        @RandomPlayer2 INT,
        @RandomGameId  INT;

SET @counter     = 1;
SET @cntPlayer   = (SELECT COUNT(*) FROM dbo.player);
SET @cntBattle   = (SELECT COUNT(*) FROM dbo.battle);
SET @cntAllGames = (SELECT COUNT(*) FROM dbo.WrkAllGames);

--Battles should be the same as in Battle table

WHILE @counter <= @cntBattle
BEGIN  

--Generate two random players
   
SET @RandomPlayer1 = FLOOR(RAND()*0.1*@cntPlayer)+1; /*take only 10% players, so some players played more games than another*/ 
SET @RandomPlayer2 = FLOOR(RAND()*0.95*@cntPlayer)+1; /*take only 95% players, so we have players that have never taken part in games*/ 
SET @RandomGameId = FLOOR(RAND()*@CntAllGames) + 1;

--Check that players are not identical

    IF @RandomPlayer1<>@RandomPlayer2

       --Insert 1 battle with two players in which one player won and another lost
    BEGIN
        INSERT INTO #tmpBattlePlayer (battleId,PlayerId,RandomGameId,PlayerBitId)
        SELECT @counter
              ,@RandomPlayer1
              ,@RandomGameId
              ,1
        UNION ALL
        SELECT @counter
              ,@RandomPlayer2
              ,@RandomGameId
              ,0 
        SET @counter = @counter + 1 
    END;
END;
 
--Insert from temporary to table to target table

TRUNCATE TABLE dbo.battleMembersXref;

INSERT INTO dbo.battleMembersXref (battleId,playerId,result)
    SELECT btl.BattleOriginalId
          ,pl.PlayerOriginalid
          ,CASE WHEN tempPl.PlayerBitId = WrkG.winner AND WrkG.winner IS NOT NULL  THEN 1
                WHEN WrkG.winner IS NULL THEN 0
                ELSE -1 
                END AS Result
    FROM #tmpBattlePlayer tempPl 
    INNER JOIN #tmpPlayerId pl ON tempPl.PlayerId = pl.PlayerSurrId
    INNER JOIN #tmpBattleId btl ON tempPl.battleId = btl.BattleSurrId
    INNER JOIN dbo.WrkAllGames WrkG ON tempPl.RandomGameId = WrkG.GameId
;

TRUNCATE TABLE dbo.battleMove;

WITH CTEunpvtAllGames AS
(
SELECT GameId, Turn, Cell  
FROM   
   (SELECT GameId, a1, b1, a2, b2, a3, b3, a4, b4, a5  
    FROM dbo.WrkAllGames) p  
UNPIVOT  
   (Cell FOR Turn IN   
      (a1, b1, a2, b2, a3, b3, a4, b4, a5))AS unpvt
),
CTEunpvtAllGamesTurnPlayer AS
(
SELECT GameId, Turn, Cell,
CASE WHEN Turn = 'a1' THEN 1
     WHEN Turn = 'b1' THEN 2
     WHEN Turn = 'a2' THEN 3
     WHEN Turn = 'b2' THEN 4
     WHEN Turn = 'a3' THEN 5
     WHEN Turn = 'b3' THEN 6
     WHEN Turn = 'a4' THEN 7
     WHEN Turn = 'b4' THEN 8
     WHEN Turn = 'a5' THEN 9
END TurnNum,
CASE WHEN Turn = 'a1' THEN 1
     WHEN Turn = 'b1' THEN 0
     WHEN Turn = 'a2' THEN 1
     WHEN Turn = 'b2' THEN 0
     WHEN Turn = 'a3' THEN 1
     WHEN Turn = 'b3' THEN 0
     WHEN Turn = 'a4' THEN 1
     WHEN Turn = 'b4' THEN 0
     WHEN Turn = 'a5' THEN 1
END PlayerBitId
FROM CTEunpvtAllGames
),
CTEBatlleMoveTime AS
(
SELECT n = 1
      ,battleId
      ,startDate
FROM dbo.battle
UNION ALL
SELECT n+1
      ,battleId
      ,DATEADD(SECOND,RAND()*60,startDate) 
FROM CTEBatlleMoveTime
WHERE n+1<=9)

INSERT INTO dbo.battleMove (moveTimeStamp,moveDescription,battleId,playerId)
    SELECT bmt.startDate
          ,unp.Cell
          ,btl.BattleOriginalId
          ,pl.PlayerOriginalid
    FROM #tmpBattlePlayer tempPl 
    INNER JOIN #tmpPlayerId pl ON tempPl.PlayerId = pl.PlayerSurrId
    INNER JOIN #tmpBattleId btl ON tempPl.battleId = btl.BattleSurrId
    INNER JOIN CTEunpvtAllGamesTurnPlayer unp ON tempPl.RandomGameId = unp.GameId AND tempPl.PlayerBitId = unp.PlayerBitId
    INNER JOIN CTEBatlleMoveTime bmt ON btl.BattleOriginalId = bmt.battleId AND bmt.n = unp.TurnNum
    ORDER BY btl.BattleOriginalId, bmt.startDate;
GO
