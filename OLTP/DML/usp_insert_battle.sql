USE online_games;

GO

IF OBJECT_ID ('dbo.usp_insert_battle') IS NOT NULL
  DROP PROCEDURE dbo.usp_insert_battle;

GO 
CREATE PROCEDURE dbo.usp_insert_battle

  @NumberOfRows INT,
  @StartDate DATE,
  @LastDate DATE 

 AS
 BEGIN


BEGIN
WITH cteCount AS
    (
    SELECT     (SELECT DATEDIFF(DAY,@StartDate,@LastDate))                           AS CntDates
              ,(SELECT DATEDIFF(ss, '2010-01-01 00:00:00', '2010-01-02 00:00:00') ) AS CntSeconds
              ,(SELECT COUNT(modetemplateid) FROM dbo.modeTemplate)                  AS CntModes
    )
	
  ,CteRecursive AS
    (
    SELECT n = 1
    UNION ALL
    SELECT n+1
    FROM CteRecursive
    WHERE n+1<=@NumberOfRows
    )
    ,CteIDGenerationSet AS
   (
    SELECT DATEADD(DAY,FLOOR(RAND(CHECKSUM(NEWID())) * ( 1 + CntDates)),@StartDate)  AS BattleDate
          ,FLOOR(RAND( CHECKSUM( NEWID()))*CntSeconds) + 1                           AS BattleTime
          ,FLOOR(RAND(CHECKSUM(NEWID()))*CntModes) + 1                               AS RandomMode
    FROM CteRecursive 
    CROSS JOIN CteCount
    
	    )
    
    INSERT INTO dbo.battle (startDate, modetemplateid)
    SELECT BattleDate + ' ' + BattleTime AS startDate, RandomMode AS modetemplateid
    FROM
    (
        SELECT LEFT(CONVERT(VARCHAR, BattleDate, 120), 10) AS BattleDate, 
               CONVERT(CHAR(8), DATEADD(SECOND, BattleTime, ''), 114)AS BattleTime,
               RandomMode            
        FROM CteIDGenerationSet
    ) T1
    ORDER BY BattleDate, BattleTime
	OPTION (MAXRECURSION 0);

END

BEGIN


IF OBJECT_ID(N'tempdb..#listofdates', N'U') IS NOT NULL   
DROP TABLE #listofdates;  

CREATE TABLE #listofdates

(
     newbattleid INT IDENTITY
    ,newstartdate DATE
);


INSERT INTO #listofdates (newstartdate)
 SELECT CONVERT(DATE, startDate) AS dateoftournament
 FROM dbo.battle
 GROUP BY CONVERT(DATE, startDate);
 

IF OBJECT_ID(N'tempdb..#listofmodes', N'U') IS NOT NULL   
DROP TABLE #listofmodes;  

CREATE TABLE #listofmodes

(
     newmodeid INT IDENTITY
    ,newmode VARCHAR (50)
);


INSERT INTO #listofmodes (newmode)
 SELECT modeTemplateName
 FROM dbo.modeTemplate;
 

IF OBJECT_ID(N'tempdb..#randomizer', N'U') IS NOT NULL   
DROP TABLE #randomizer; 


CREATE TABLE #randomizer
(
  randomdateid INT
 ,randommodeid INT
);

DECLARE @counter         INT,
        @cntdate         INT,
        @cntmodes        INT,
        @RandomModeID    INT,
        @RandomDateID    INT;

SET    @counter = 1
SET    @cntdate = (SELECT COUNT(*) FROM #listofdates);
SET    @cntmodes = (SELECT COUNT(*) FROM #listofmodes);

   WHILE @counter <= 100
   BEGIN  
   
   SET @RandomDateID = FLOOR(RAND()*@cntdate)+1
       IF @RandomDateID NOT IN (SELECT randomdateid FROM #randomizer)

       BEGIN
       INSERT INTO #randomizer (randomdateid, randommodeid)
       SELECT @RandomDateID, (FLOOR(RAND( CHECKSUM( NEWID()))*@cntmodes) + 1) AS  randommodeid
       SET @counter = @counter + 1  
       END
       
   END

  MERGE    dbo.battle AS TARGET
  USING 
  (
  SELECT bat.battleId, bat.startDate, m.newmodeid AS modetemplateid, DENSE_RANK () OVER (ORDER BY t.newstartdate) AS tournamentid
  FROM #randomizer AS r
  INNER JOIN #listofdates AS t ON r.randomdateid = t.newbattleid
  INNER JOIN #listofmodes AS m ON r.randommodeid = m.newmodeid
  INNER JOIN dbo.battle bat ON    T.newstartdate =  CONVERT(date, bat.startDate)
  )
  AS SOURCE
  ON (TARGET.battleId = SOURCE.battleId)
  WHEN MATCHED AND  ISNULL(TARGET.tournamentid,0) <> SOURCE.tournamentId OR ISNULL(TARGET.modetemplateid,0) <> SOURCE.modetemplateid
  THEN UPDATE SET  TARGET.tournamentid = SOURCE.tournamentid, TARGET.modetemplateid = SOURCE.modetemplateid;
  
  END
  
END;
GO

