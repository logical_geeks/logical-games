USE online_games;

GO

IF OBJECT_ID ( 'dbo.usp_insert_privateChat' ) IS NOT NULL   
DROP PROCEDURE dbo.usp_insert_PrivateChat;  
GO  

CREATE PROCEDURE dbo.usp_insert_privateChat
    @NumberOfCharts INT
AS   

BEGIN

IF OBJECT_ID(N'tempdb..#tmpPl', N'U') IS NOT NULL   
DROP TABLE #tmpPl;  

CREATE TABLE #tmpPl
(
id          INT IDENTITY,
originalid  INT
);

INSERT INTO #tmpPl (originalid)
SELECT playerid
FROM dbo.player;

WITH CountPlayers AS
(
SELECT COUNT (playerId) AS CntPlayer FROM dbo.player 
),

CteRecursive AS
    (
    SELECT n = 1
    UNION ALL
    SELECT n+1
    FROM CteRecursive WHERE n+1<=@NumberOfCharts
    )

INSERT INTO dbo.privateChat (firstPlayerId, secondPlayerId)
SELECT tmp1.originalid,
       tmp2.originalid
FROM
(
SELECT FLOOR(RAND( CHECKSUM( NEWID()))*CntPlayer) + 1 RandomPlayer1,
       FLOOR(RAND( CHECKSUM( NEWID()))*CntPlayer) + 1 RandomPlayer2
FROM CteRecursive
CROSS JOIN CountPlayers
) rndPlayers 
LEFT JOIN #tmpPl tmp1 ON rndPlayers.RandomPlayer1 = tmp1.id
LEFT JOIN #tmpPl tmp2 ON rndPlayers.RandomPlayer2 = tmp2.id

OPTION (MAXRECURSION 0)

END;

GO
