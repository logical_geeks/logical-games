
USE online_games;

GO

IF OBJECT_ID ( 'dbo.usp_insert_tournament', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.usp_insert_tournament;  
GO  

CREATE PROCEDURE dbo.usp_insert_tournament
 
AS   

BEGIN

SET IDENTITY_INSERT dbo.tournament ON; 
WITH CteCount AS

    (
    SELECT (SELECT COUNT(1) 
    FROM dbo.tournamentStatusTemplate) AS CntTemplates
    
    ), tournamentStatusTemplates AS
 
(
SELECT  ROW_NUMBER () OVER (ORDER BY tournamentStatusTemplateid) AS TempID, tournamentStatusTemplateName
FROM dbo.tournamentStatusTemplate
)
, CteRecursive AS

(
SELECT n = 1
UNION ALL
SELECT n+1
FROM CteRecursive
WHERE n+1<=100
)

, CteIDGenerationSet AS
(
SELECT FLOOR(RAND(CHECKSUM(NEWID()))*CntTemplates) + 1 AS RandomTournamentStatusTenplateID,
       ROW_NUMBER () OVER (ORDER BY FLOOR(RAND(CHECKSUM(NEWID()))*CntTemplates) + 1) AS Rn_Temp
FROM CteRecursive 
CROSS JOIN CteCount
)
, CTEBattle AS
(
SELECT tournamentid 
      , ROW_NUMBER () OVER (ORDER BY tournamentid) AS Rn_tour
      ,'tic-tac-toe'+''+ CONVERT(VARCHAR(10), (MIN(CONVERT(date, startDate))),120) AS tournamentname
      ,MIN(startdate) as startdate 
      ,4 AS minMemberCourt
      ,modeTemplateId
FROM  dbo.battle 
WHERE tournamentId IS NOT NULL
GROUP BY tournamentid, modeTemplateId
)
INSERT INTO dbo.tournament (tournamentId, tournamentName, startDate, minMemberCount, modeTemplateId, tournamentStatusTemplateId)
SELECT    tournamentid, tournamentname, startdate, minMemberCourt, modetemplateid, RandomTournamentStatusTenplateID
FROM CTEBattle AS b
INNER JOIN CteIDGenerationSet AS g
ON G.Rn_Temp = B.Rn_tour;

SET IDENTITY_INSERT dbo.tournament OFF; 

END

