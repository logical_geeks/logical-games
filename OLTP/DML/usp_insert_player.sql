USE online_games;

GO

IF OBJECT_ID ('dbo.usp_insert_player' ) IS NOT NULL   
    DROP PROCEDURE dbo.usp_insert_player;  
GO  


CREATE PROCEDURE dbo.usp_insert_player
   
    @NumberOfRows INT,  
    @StartAgeDate DATE,
	@LastAgeDate DATE 
AS   

BEGIN

WITH CteCount AS
    --Count rows for First, Last name and period of possible birth date
    (
    SELECT (SELECT COUNT(1) FROM dbo.WrkFirstName)          AS CntFirstName
          ,(SELECT COUNT(1) FROM dbo.WrkLastName)           AS CntLastName
          ,(SELECT DATEDIFF(DAY,@StartAgeDate,@LastAgeDate))AS CntBirthDates
          ,(SELECT COUNT(id) FROM dbo.Worldcities)          AS CntCities

    ),

    --Generate new surrogate id for First name
     CteNewIDFirstName AS
    (
    SELECT ROW_NUMBER () OVER (ORDER BY firstNameID) AS FNameID,
            firstName
    FROM dbo.WrkFirstName
    ),

    --Generate new surrogate id for Last name
     CteNewIDLastName AS
    (
    SELECT ROW_NUMBER () OVER (ORDER BY lastNameID) AS LNameID,
            lastName
    FROM dbo.WrkLastName
    ),
    
     --Generate new surrogate id for cities
     CteCities AS 
    (
    SELECT ROW_NUMBER() OVER ( ORDER BY city_ascii) as CnamID,
           city_ascii, country
    FROM dbo.worldcities
    ),

    --Generate required amount of rows for final result set
     CteRecursive AS
    (
    SELECT n = 1
    UNION ALL
    SELECT n+1
    FROM CteRecursive
    WHERE n+1<=@NumberOfRows
    ),

    --Get random first and last names, birth dates
     CteIDGenerationSet AS
    (
    SELECT FLOOR(RAND(CHECKSUM(NEWID()))*CntFirstName) + 1                                   AS RandomFN
          ,FLOOR(RAND(CHECKSUM(NEWID()))*CntLastName) + 1                                    AS RandomLN
          ,FLOOR(RAND( CHECKSUM( NEWID()))*CntCities) + 1                                    AS RandomCity
          ,DATEADD(DAY,FLOOR(RAND(CHECKSUM(NEWID())) * ( 1 + CntBirthDates)),@StartAgeDate)  AS BirthDate
    FROM CteRecursive 
    CROSS JOIN CteCount
    ),

    --Get possible first registration dates (assume the minimum age of registration and first registration was '2010-05-02')
    CteRegDateGenerationSet AS
    (
    SELECT RandomFN
          ,RandomLN
          ,BirthDate
          ,RandomCity
          ,CASE 
           WHEN DATEADD(YEAR,DATEDIFF(YEAR,@LastAgeDate,GETDATE()),BirthDate) <= '2010-05-02' THEN '2010-05-02'
           WHEN DATEADD(YEAR,DATEDIFF(YEAR,@LastAgeDate,GETDATE()),BirthDate) >  '2010-05-02' THEN DATEADD(YEAR,DATEDIFF(YEAR,@LastAgeDate,GETDATE()),BirthDate)
           END AS PossibleStartRegistrDate
    FROM CteIDGenerationSet
    ),

    --Generate registration date
    CteIDGenerationSetFinal AS
    (
    SELECT RandomFN
          ,RandomLN
          ,BirthDate
          ,RandomCity
          ,DATEADD(DAY, FLOOR(RAND(CHECKSUM(NEWID())) * ( 1 + DATEDIFF(DAY,PossibleStartRegistrDate,GETDATE()))), PossibleStartRegistrDate) AS RegistrDate
    FROM CteRegDateGenerationSet
    )

	INSERT INTO dbo.player (userName, userPassword, birthDate, city,country, registrationDate)
    SELECT FN.firstName + '_' + LN.lastName AS userName
          ,SUBSTRING(CONVERT(VARCHAR(40), NEWID()),0,9) as userPassword 
          ,dte.birthDate
          ,СY.city_ascii AS city
		  ,СY.country 
          ,dte.registrDate AS registrationDate
    FROM CteIDGenerationSetFinal dte
    LEFT JOIN CteNewIDFirstName FN ON dte.RandomFN = FN.FNameID
    LEFT JOIN CteNewIDLastName  LN ON dte.RandomLN = LN.LNameID
    LEFT JOIN CteCities         СY ON dte.RandomCity = СY.CnamID

OPTION (MAXRECURSION 0)


END;
GO

