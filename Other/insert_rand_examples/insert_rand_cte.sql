USE online_games;

GO

IF OBJECT_ID(N'tempdb..#tmp', N'U') IS NOT NULL   
DROP TABLE #tmp;  

GO


CREATE TABLE #tmp
(
id			INT IDENTITY,
originalid  INT
);

GO

INSERT INTO #tmp (originalid)
SELECT playerid
FROM dbo.player;

GO

--Minus: NEWID

WITH cte_recursive AS
(SELECT n = 1, 
		m = 1 
UNION ALL
SELECT n+1, 
	   m
FROM cte_recursive WHERE n+1<=10)

INSERT INTO dbo.privateChat (firstPlayerId, secondPlayerId)
SELECT m*(FLOOR(RAND( CHECKSUM( NEWID()))*@@ROWCOUNT) + 1) Random_Number,
	   m*(FLOOR(RAND( CHECKSUM( NEWID()))*@@ROWCOUNT) + 1) Random_Number_2
FROM cte_recursive;

GO