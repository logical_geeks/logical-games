USE online_games;

GO

IF OBJECT_ID(N'tempdb..#tmp', N'U') IS NOT NULL   
DROP TABLE #tmp;  

GO


CREATE TABLE #tmp
(
id			INT IDENTITY,
originalid  INT
);

GO

INSERT INTO #tmp (originalid)
SELECT playerid
FROM dbo.player;

GO

--Minus: NEWID

DECLARE @json NVARCHAR(MAX)
DECLARE @cnt  INT; 

SET @cnt  = (SELECT COUNT(*) FROM dbo.player) 
SET @json = N'[1,2,3,4,5,6,7,8,9,10]'

INSERT INTO dbo.privateChat (firstPlayerId, secondPlayerId)   
SELECT FLOOR(RAND( CHECKSUM( NEWID()))*@cnt) + 1,
	   FLOOR(RAND( CHECKSUM( NEWID()))*@cnt) + 1  
FROM OPENJSON(@json);

GO