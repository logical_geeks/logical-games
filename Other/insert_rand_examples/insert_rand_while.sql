USE online_games;

GO

IF OBJECT_ID(N'tempdb..#tmp', N'U') IS NOT NULL   
DROP TABLE #tmp;  

GO


CREATE TABLE #tmp
(
id			INT IDENTITY,
originalid  INT
);

GO

INSERT INTO #tmp (originalid)
SELECT playerid
FROM dbo.player;

GO

IF OBJECT_ID(N'tempdb..#privateChartTMP', N'U') IS NOT NULL   
DROP TABLE #privateChartTMP;  

GO

CREATE TABLE #privateChartTMP
(
firstPlayerId	INT,
secondPlayerId  INT
);

GO

--Minus: evety insert is a new transaction

DECLARE @counter SMALLINT,
		@cnt	 INT; 


SET	@counter = 1
SET @cnt	 = (SELECT COUNT(*) FROM dbo.player) 


WHILE @counter <= 10
   BEGIN  
	  INSERT INTO #privateChartTMP (firstPlayerId,secondPlayerId)
      SELECT FLOOR(RAND()*@cnt)+1 Random_Number, 
			 FLOOR(RAND()*@cnt)+1 Random_Number_2   
      SET @counter = @counter + 1  
   END;  

   INSERT INTO dbo.privateChat (firstPlayerId,secondPlayerId)
      SELECT firstPlayerId,
			 secondPlayerId  
	  FROM 	#privateChartTMP

GO  