USE online_games;
GO

IF OBJECT_ID(N'dbo.WrkAllGames', N'U') IS NOT NULL
DROP TABLE dbo.AllGames;  

CREATE TABLE dbo.WrkAllGames
(
    GameId INT     NOT NULL IDENTITY (1,1),
    a1     TINYINT NOT NULL,
    b1     TINYINT NOT NULL,
    a2     TINYINT NOT NULL,
    b2     TINYINT NOT NULL,
    a3     TINYINT NOT NULL,
    b3     TINYINT NOT NULL,
    a4     TINYINT NOT NULL,
    b4     TINYINT NOT NULL,
    a5     TINYINT NOT NULL,
    winner BIT -- NULL = none; 0 = A, 1 = B
);
GO

IF OBJECT_ID(N'dbo.CalcWinList', N'IF') IS NOT NULL
DROP TABLE dbo.CalcWinList;
GO

CREATE FUNCTION dbo.CalcWinList()
RETURNS TABLE
AS RETURN
(
    SELECT
        WinningBitmap = WinList.WinningBitmap
    FROM        (
                    SELECT POWER(2,1) + POWER(2,2) + POWER(2,3) UNION ALL
                    SELECT POWER(2,4) + POWER(2,5) + POWER(2,6) UNION ALL
                    SELECT POWER(2,7) + POWER(2,8) + POWER(2,9) UNION ALL
                    SELECT POWER(2,1) + POWER(2,4) + POWER(2,7) UNION ALL
                    SELECT POWER(2,2) + POWER(2,5) + POWER(2,8) UNION ALL
                    SELECT POWER(2,3) + POWER(2,6) + POWER(2,9) UNION ALL
                    SELECT POWER(2,1) + POWER(2,5) + POWER(2,9) UNION ALL
                    SELECT POWER(2,3) + POWER(2,5) + POWER(2,7)
                ) AS WinList(WinningBitmap)
);
GO


IF OBJECT_ID(N'dbo.CalcTTTPlayerBitmap', N'IF') IS NOT NULL
DROP TABLE dbo.CalcTTTPlayerBitmap;
GO

CREATE FUNCTION CalcTTTPlayerBitmap
(
    @player bit, /* player a = 1; player b = 0 */
    @turn tinyint,
    @a1 tinyint,
    @b1 tinyint,
    @a2 tinyint,
    @b2 tinyint,
    @a3 tinyint,
    @b3 tinyint,
    @a4 tinyint,
    @b4 tinyint,
    @a5 tinyint
)
RETURNS TABLE
AS RETURN
(
    SELECT
        PlayBitmap =
            CASE WHEN @player = 1 AND @turn >= 1 AND @a1 IS NOT NULL THEN POWER(2,@a1) ELSE 0 END +
            CASE WHEN @player = 0 AND @turn >= 1 AND @b1 IS NOT NULL THEN POWER(2,@b1) ELSE 0 END +
            CASE WHEN @player = 1 AND @turn >= 2 AND @a2 IS NOT NULL THEN POWER(2,@a2) ELSE 0 END +
            CASE WHEN @player = 0 AND @turn >= 2 AND @b2 IS NOT NULL THEN POWER(2,@b2) ELSE 0 END +
            CASE WHEN @player = 1 AND @turn >= 3 AND @a3 IS NOT NULL THEN POWER(2,@a3) ELSE 0 END +
            CASE WHEN @player = 0 AND @turn >= 3 AND @b3 IS NOT NULL THEN POWER(2,@b3) ELSE 0 END +
            CASE WHEN @player = 1 AND @turn >= 4 AND @a4 IS NOT NULL THEN POWER(2,@a4) ELSE 0 END +
            CASE WHEN @player = 0 AND @turn >= 4 AND @b4 IS NOT NULL THEN POWER(2,@b4) ELSE 0 END +
            CASE WHEN @player = 1 AND @turn >= 5 AND @a5 IS NOT NULL THEN POWER(2,@a5) ELSE 0 END
);
GO

IF OBJECT_ID(N'dbo.CalcTTTWin', N'IF') IS NOT NULL
DROP TABLE dbo.CalcTTTWin;
GO

CREATE FUNCTION dbo.CalcTTTWin
(
    @a1 tinyint,
    @b1 tinyint,
    @a2 tinyint,
    @b2 tinyint,
    @a3 tinyint,
    @b3 tinyint,
    @a4 tinyint,
    @b4 tinyint,
    @a5 tinyint
)
RETURNS TABLE
AS RETURN
(
    SELECT
        TOP 1
        Winner = CONVERT(bit,Players.Player)
    FROM        (
                    SELECT 1 UNION ALL SELECT 0
                ) AS Players(Player)
    CROSS JOIN    (
                    /* We could omit 1 and 2 here, as we know no one can win before the third round. */
                    --SELECT 1 UNION ALL
                    --SELECT 2 UNION ALL
                    SELECT 3 UNION ALL
                    SELECT 4 UNION ALL
                    SELECT 5
                ) AS Turns(Turn)
    CROSS JOIN    CalcWinList() AS WinBitmaps
    CROSS APPLY    CalcTTTPlayerBitmap
                (
                    Players.Player,
                    Turns.Turn,
                    @a1,
                    @b1,
                    @a2,
                    @b2,
                    @a3,
                    @b3,
                    @a4,
                    @b4,
                    @a5
                ) AS PlayerBitMap
    WHERE        PlayerBitMap.PlayBitmap & WinBitmaps.WinningBitmap = WinBitmaps.WinningBitmap
    ORDER BY    Turns.Turn ASC,
                /*
                    Player 1 is X; Player 2 (O) is represented as 0 here.
                    Player 1 goes first, which is why this is DESC, because 1 > 0.
                */
                Players.Player DESC
);

GO


WITH g AS
(
    SELECT n = 1 
    UNION ALL
    SELECT n+1
    FROM g
    WHERE n+1 <= 9 
)
INSERT INTO dbo.WrkAllGames
(
    a1, b1, a2, b2, a3, b3, a4, b4, a5, winner
)
SELECT
    a1 = g.n,
    b1 = g2.n,
    a2 = g3.n,
    b2 = g4.n,
    a3 = g5.n,
    b3 = g6.n,
    a4 = g7.n,
    b4 = g8.n,
    a5 = g9.n,
    Winner.Winner
FROM        g
JOIN        g g2    ON g2.n NOT IN (g.n)
JOIN        g g3    ON g3.n NOT IN (g.n, g2.n)
JOIN        g g4    ON g4.n NOT IN (g.n, g2.n, g3.n)
JOIN        g g5    ON g5.n NOT IN (g.n, g2.n, g3.n, g4.n)
JOIN        g g6    ON g6.n NOT IN (g.n, g2.n, g3.n, g4.n, g5.n)
JOIN        g g7    ON g7.n NOT IN (g.n, g2.n, g3.n, g4.n, g5.n, g6.n)
JOIN        g g8    ON g8.n NOT IN (g.n, g2.n, g3.n, g4.n, g5.n, g6.n, g7.n)
JOIN        g g9    ON g9.n NOT IN (g.n, g2.n, g3.n, g4.n, g5.n, g6.n, g7.n, g8.n)

OUTER APPLY    CalcTTTWin
            (
                g.n,
                g2.n,
                g3.n,
                g4.n,
                g5.n,
                g6.n,
                g7.n,
                g8.n,
                g9.n
            ) AS Winner;