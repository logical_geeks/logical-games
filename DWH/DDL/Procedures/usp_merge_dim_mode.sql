USE online_games; 

GO 

IF Object_id ('dbo.usp_merge_dim_mode') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_dim_mode; 

GO 

CREATE PROCEDURE dbo.Usp_merge_dim_mode 
AS 
  BEGIN 
      MERGE dbo.dim_mode AS TARGET 
      USING (SELECT ISNULL(modetemplateid, -1) AS Mode_ID,      
                    ISNULL(LTRIM(RTRIM(modetemplatename)),'Unknown') AS Mode_Name 
             FROM   dbo.modetemplate) AS SOURCE 
      ON ( TARGET.mode_id = SOURCE.mode_id ) 
      WHEN MATCHED AND TARGET.mode_name <> SOURCE.mode_name THEN 
        UPDATE SET TARGET.mode_name = SOURCE.mode_name 
      WHEN NOT MATCHED BY target THEN 
        INSERT (mode_id, 
                mode_name) 
        VALUES (mode_id, 
                mode_name) 
      WHEN NOT MATCHED BY SOURCE THEN 
        DELETE; 
  END; 