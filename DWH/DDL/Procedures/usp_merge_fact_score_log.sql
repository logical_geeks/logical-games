USE online_games;
GO 


IF OBJECT_ID ('dbo.usp_merge_fact_score_log') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_fact_score_log; 

GO 

CREATE PROCEDURE dbo.usp_merge_fact_score_log
AS 
  BEGIN 

-- read LSN from the previous load
DECLARE @lsn BINARY(10) = 
( 
            SELECT MAX(lsn) 
            FROM   dbo.log_trg_cdc 
            WHERE  table_name = 'dbo.Fact_Score_Log' 
); 

-- load process
WITH cte_incr_load AS 


( 
               SELECT   ROW_NUMBER() OVER ( PARTITION BY battleid ORDER BY __$start_lsn DESC, __$seqval DESC ) AS __$rn,
                        * 
               FROM     [cdc].[dbo_battle_CT] 
               WHERE    __$operation = 2 
               AND      __$start_lsn > @lsn 
) 
  
     , cte_full_load AS 
( 
              SELECT * 
              FROM   dbo.battle 
              WHERE  @lsn IS NULL ) 

	 , cte_union AS 
 ( 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     __$operation 
              FROM   cte_incr_load AS cte_l 
              WHERE  cte_l.__$rn = 1 
              UNION ALL 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     2 AS __$operation 
              FROM   cte_full_load )
MERGE dbo.Fact_Score_Log AS TARGET 
USING       ( 
                       SELECT b.battleid as battle_id, DD.Date_Key, DT.Time_Key, DP.Player_Key, BM.result AS Scored_Points,  cte.__$operation 
					   FROM dbo.battle AS B
					   INNER JOIN cte_union AS cte ON B.battleId = cte.battleid
					   INNER JOIN dbo.Dim_Date AS DD ON CAST(B.startdate AS DATE) = DD.fulldate 
					   INNER JOIN dbo.Dim_Time AS DT ON CONVERT(VARCHAR(8), B.startdate, 108) = DT.fulltimeext 
					   INNER JOIN dbo.battleMembersXref AS BM ON B.battleId = BM.battleId
					   INNER JOIN dbo.dim_player AS DP ON BM.playerid = DP.player_id 
					   

			  ) AS SOURCE 
ON TARGET.battle_id = SOURCE.battle_id 
WHEN NOT matched BY TARGET THEN 
INSERT 
       ( 
              battle_id, 
              Date_Key, 
              Time_Key, 
              Player_Key, 
              Scored_Points
       ) 
       VALUES 
       ( 
              battle_id, 
              Date_Key, 
              Time_Key, 
              Player_Key, 
              Scored_Points
       ); 

-- mark the end of the load process and the latest LSN
INSERT dbo.log_trg_cdc 
       ( 
              table_name, 
              lsn 
       ) 
       VALUES 
       ( 
              'dbo.Fact_Score_Log', 
               ISNULL((SELECT MAX(__$start_lsn) FROM [cdc].[dbo_battle_ct]),0) 
       )
END

