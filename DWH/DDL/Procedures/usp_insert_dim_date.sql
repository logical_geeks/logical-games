IF OBJECT_ID ('dbo.usp_insert_dim_date') IS NOT NULL 
  DROP PROCEDURE dbo.usp_insert_dim_date; 

GO 

CREATE PROCEDURE dbo.Usp_insert_dim_date @StartDate DATE = '2000-01-01', 
                                         @EndDate   DATE 
AS 
  BEGIN 
      DECLARE @maxdate DATE; 
      DECLARE @mindate DATE; 

        SELECT TOP 1 @maxdate = fulldate 
        FROM   dbo.dim_date 
        ORDER  BY fulldate DESC 

       
	    SELECT TOP 1 @mindate = fulldate 
        FROM   dbo.dim_date 
        ORDER  BY fulldate 

      IF      (( @maxdate IS NOT NULL) 
           AND ( @maxdate < @EndDate ) 
           AND ( @mindate > @StartDate )) 
        BEGIN 
            WITH ctedays (d) 
                 AS (SELECT DATEADD (DAY, 1, @maxdate) 
                     UNION ALL 
                     SELECT DATEADD(DAY, 1, d) 
                     FROM   ctedays 
                     WHERE  d < @EndDate) 
            INSERT INTO dim_date 
                        (date_key, 
                         fulldate, 
                         dayofmonthvalue, 
                         daynamevalue, 
                         weekofmonth, 
                         weekofquarter, 
                         weekofyear, 
                         monthnumber, 
                         monthnamevalue, 
                         monthofquarter, 
                         quartervalue, 
                         semester, 
                         yearvalue) 
            SELECT CAST(CONVERT(VARCHAR(20), d, 112) AS INT) AS DateKey,   
			       d AS FullDate, 
                   DATEPART(DAY, d) AS DayOfMonthValue, 
                   DATENAME(WEEKDAY, d) AS DayNameValue, 
                   DATEDIFF(WEEK, 0, d) - (DATEDIFF(WEEK, 0, DATEADD(dd, -DAY(d)+ 1, d)) - 1 ) AS WeekOfMonth, 
                 ( DATEDIFF(DAY, DATEADD(QUARTER, DATEDIFF(QUARTER, 0, d), 0), d) / 7 ) + 1 AS WeekOfQuarter, 
                   DATEPART(WEEK, d) AS WeekOfYear, 
                   DATEPART(MONTH, d) AS MonthNumber, 
                   DATENAME(MONTH, d) AS MonthNameValue, 
                   CASE 
                     WHEN DATEPART(MONTH, d) IN ( 1, 4, 7, 10 ) THEN 1 
                     WHEN DATEPART(MONTH, d) IN ( 2, 5, 8, 11 ) THEN 2 
                     WHEN DATEPART(MONTH, d) IN ( 3, 6, 9, 12 ) THEN 3 
                   END 
                   AS MonthOfQuarter, 
                   DATEPART(QUARTER, d) AS QuarterValue, 
                   CASE 
                     WHEN DATEPART(QUARTER, d) >= 3 THEN 2 
                     ELSE 1 
                   END 
                   AS Semester, 
                   DATEPART(YEAR, d) AS YearValue 
            FROM   ctedays 
            OPTION (maxrecursion 0); 
        END 
      ELSE 
        BEGIN 
            TRUNCATE TABLE dbo.dim_date; 
            WITH ctedays (d) 
                 AS (SELECT @startdate 
                     UNION ALL 
                     SELECT DATEADD(DAY, 1, d) 
                     FROM   ctedays 
                     WHERE  d < @EndDate) 
            INSERT INTO dim_date 
                        (date_key, 
                         fulldate, 
                         dayofmonthvalue, 
                         daynamevalue, 
                         weekofmonth, 
                         weekofquarter, 
                         weekofyear, 
                         monthnumber, 
                         monthnamevalue, 
                         monthofquarter, 
                         quartervalue, 
                         semester, 
                         yearvalue) 
            SELECT CAST(CONVERT(VARCHAR(20), d, 112) AS INT) AS DateKey, 
                   d AS FullDate, 
                   DATEPART(DAY, d) AS DayOfMonthValue, 
                   DATENAME(WEEKDAY, d) AS DayNameValue, 
                   DATEDIFF(WEEK, 0, d) - (DATEDIFF(WEEK, 0, DATEADD(dd, -DAY(d) + 1, d ))- 1 ) AS WeekOfMonth, 
                   (DATEDIFF(DAY, DATEADD(QUARTER, DATEDIFF(QUARTER, 0, d), 0), d) / 7 ) + 1 AS WeekOfQuarter, 
                   DATEPART(WEEK, d) AS WeekOfYear, 
                   DATEPART(MONTH, d) AS MonthNumber, 
                   DATENAME(MONTH, d) AS MonthNameValue, 
                   CASE 
                     WHEN DATEPART(MONTH, d) IN ( 1, 4, 7, 10 ) THEN 1 
                     WHEN DATEPART(MONTH, d) IN ( 2, 5, 8, 11 ) THEN 2 
                     WHEN DATEPART(MONTH, d) IN ( 3, 6, 9, 12 ) THEN 3 
                   END 
                   AS MonthOfQuarter, 
                   DATEPART(QUARTER, d) AS QuarterValue, 
                   CASE 
                     WHEN DATEPART(QUARTER, d) >= 3 THEN 2 
                     ELSE 1 
                   END 
                   AS Semester, 
                   DATEPART(YEAR, d) AS YearValue 
            FROM   ctedays 
            OPTION (maxrecursion 0); 
        END 
  END 