USE online_games; 

GO 

IF OBJECT_ID ('dbo.usp_insert_dim_time') IS NOT NULL 
  DROP PROCEDURE dbo.usp_insert_dim_time; 

GO 

CREATE PROCEDURE dbo.Usp_insert_dim_time 
AS 
  BEGIN 
      TRUNCATE TABLE dbo.dim_time; 

      DECLARE @HourSize INT, 
              @MinSize  INT, 
              @SecSize  INT 

      SET @HourSize=23; 
      SET @MinSize=59; 
      SET @SecSize=59; 

      WITH cterechours 
           AS (SELECT h = 0 
               UNION ALL 
               SELECT h + 1 
               FROM   cterechours 
               WHERE  h + 1 <= @HourSize), 
           cterecmin 
           AS (SELECT m = 0 
               UNION ALL 
               SELECT m + 1 
               FROM   cterecmin 
               WHERE  m + 1 <= @MinSize), 
           cterecsec 
           AS (SELECT s = 0 
               UNION ALL 
               SELECT s + 1 
               FROM   cterecsec 
               WHERE  s + 1 <= @SecSize), 
           generationset 
           AS (SELECT CASE 
                        WHEN h < 10 THEN '0' + Cast(h AS VARCHAR(10)) 
                        WHEN h >= 10 THEN Cast(h AS VARCHAR(10)) 
                      END AS hournumber, 
                      CASE 
                        WHEN m < 10 THEN '0' + Cast(m AS VARCHAR(10)) 
                        WHEN m >= 10 THEN Cast(m AS VARCHAR(10)) 
                      END AS minutenumber, 
                      CASE 
                        WHEN s < 10 THEN '0' + Cast(s AS VARCHAR(10)) 
                        WHEN s >= 10 THEN Cast(s AS VARCHAR(10)) 
                      END AS secondenumber, 
                      h   AS purehournumber 
               FROM   cterechours 
                      CROSS JOIN cterecmin 
                      CROSS JOIN cterecsec) 
      INSERT INTO dim_time 
                  (fulltime, 
                   fulltimeext, 
                   hournumber, 
                   timeinseconds, 
                   timegroupkey, 
                   timegroup) 
      SELECT hournumber * 10000 + minutenumber * 100 + secondenumber AS FullTime, 
             hournumber + ':' + minutenumber + ':' + secondenumber AS FullTimeExtended, 
             purehournumber AS HourNumber, 
             hournumber * 3600 + minutenumber * 60 + secondenumber AS TimeInSeconds, 
             CASE 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 00000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 25959 ) 
             THEN 0 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 30000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 65959 ) 
             THEN 1 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 70000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 85959 ) 
             THEN 2 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 90000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 115959 ) 
             THEN 3 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >=  120000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 135959 ) 
             THEN 4 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 140000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 155959 ) 
             THEN 5 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 50000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 175959 ) 
             THEN 6 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 180000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 235959 ) 
             THEN 7 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 240000 ) 
             THEN 8 
             END AS TimeGroupKey, 
             CASE 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 00000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 25959 ) 
             THEN 'Late Night (00:00 AM To 02:59 AM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 30000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 65959 ) 
             THEN 'Early Morning(03:00 AM To 6:59 AM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 70000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 85959 ) 
             THEN 'AM Peak (7:00 AM To 8:59 AM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 90000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 115959 ) 
             THEN 'Mid Morning (9:00 AM To 11:59 AM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 120000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 135959 ) 
             THEN 'Lunch (12:00 PM To 13:59 PM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 140000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 155959 ) 
             THEN 'Mid Afternoon (14:00 PM To 15:59 PM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 50000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 175959 ) 
             THEN 'PM Peak (16:00 PM To 17:59 PM)' 
               WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 180000 
                      AND hournumber * 10000 + minutenumber * 100 + secondenumber <= 235959 ) 
             THEN 'Evening (18:00 PM To 23:59 PM)' 
			   WHEN ( hournumber * 10000 + minutenumber * 100 + secondenumber >= 240000 ) 
             THEN 'Previous Day Late Night (24:00 PM to ' + CAST( @HourSize AS VARCHAR(10)) + ':00 PM )' 
             END AS TimeGroup 
      FROM   generationset 
      ORDER  BY hournumber, 
                minutenumber, 
                secondenumber 
  END; 