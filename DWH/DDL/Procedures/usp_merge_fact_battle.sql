USE online_games; 

GO 

IF OBJECT_ID ('dbo.usp_merge_fact_battle') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_fact_battle; 

GO 

CREATE PROCEDURE dbo.Usp_merge_fact_battle 
AS 
  BEGIN 
  -- read LSN from the previous load
DECLARE @lsn BINARY(10) = 
( 
              SELECT MAX(lsn) 
              FROM   dbo.log_trg_cdc 
              WHERE  table_name = 'dbo.Fact_Battle' 
); 
-- load process
WITH cte_incr_load AS 


( 
               SELECT   ROW_NUMBER() OVER ( PARTITION BY battleid ORDER BY __$start_lsn DESC, __$seqval DESC ) AS __$rn,
                        * 
               FROM     [cdc].[dbo_battle_CT] 
               WHERE    __$operation = 2 
               AND      __$start_lsn > @lsn 
) 
  
     , cte_full_load AS 
( 
              SELECT * 
              FROM   dbo.battle 
              WHERE  @lsn IS NULL ) 

	 , cte_union AS 
 ( 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     __$operation 
              FROM   cte_incr_load AS cte_l 
              WHERE  cte_l.__$rn = 1 
              UNION ALL 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     2 AS __$operation 
              FROM   cte_full_load )

	
     MERGE dbo.fact_battle AS TARGET 
     USING (
	          SELECT       B.battleid AS battle_id, 
				           DD.date_key AS Date_Key,
                           DT.time_key AS Time_Key,
                           DP.player_key,
						   B.movedescription 
              FROM   dbo.battlemove AS B 
			  INNER JOIN cte_union AS cte ON B.battleId = cte.battleid
              INNER JOIN dbo.dim_player AS DP ON B.playerid = DP.player_id
			  INNER JOIN dbo.dim_date AS DD ON  CAST(B.movetimestamp AS DATE) = DD.fulldate 
              INNER JOIN dbo.dim_time AS DT ON  CONVERT(VARCHAR(8), B.movetimestamp, 108) = DT.fulltimeext 
	 ) AS SOURCE 
     ON TARGET.battle_id = SOURCE.battle_id 
            WHEN NOT matched BY TARGET THEN 
	INSERT (battle_id,
	        date_key, 
            time_key, 
			player_key,
            movedescription
            ) 
    VALUES (battle_id,
	        date_key, 
            time_key, 
			player_key,
            ISNULL(movedescription, -1));
-- mark the end of the load process and the latest LSN
INSERT dbo.log_trg_cdc 
       ( 
              table_name, 
              lsn 
       ) 
       VALUES 
       ( 
              'dbo.Fact_Battle', 
               ISNULL((SELECT MAX(__$start_lsn) FROM [cdc].[dbo_battle_ct]),0) 
       )
  END
