USE online_games; 

GO 

IF OBJECT_ID ('dbo.usp_merge_dim_game ') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_dim_game ; 

GO 

CREATE PROCEDURE dbo.usp_merge_dim_game 
AS 
  BEGIN 
      MERGE dbo.Dim_Game AS TARGET 
      USING (SELECT gametemplateid AS Game_ID,  
                    ISNULL(LTRIM(RTRIM(gametemplatename)),'Unknown')  AS gametemplatename
             FROM   dbo.gametemplate) AS SOURCE 
      ON ( TARGET.Game_ID = SOURCE.Game_ID ) 
      WHEN MATCHED AND TARGET.game_name <> SOURCE.gametemplatename THEN 
        UPDATE SET TARGET.game_name = SOURCE.gametemplatename 
      WHEN NOT MATCHED BY TARGET THEN 
        INSERT (game_id, 
                game_name) 
        VALUES (game_id, 
                gametemplatename)
      WHEN NOT MATCHED BY SOURCE THEN 
        DELETE; 
  END 