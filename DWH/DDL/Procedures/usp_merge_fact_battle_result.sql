USE online_games;
GO 


IF OBJECT_ID ('dbo.usp_merge_fact_battle_result') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_fact_battle_result; 

GO 

CREATE PROCEDURE dbo.usp_merge_fact_battle_result 
AS 
  BEGIN 

-- read LSN from the previous load
DECLARE @lsn BINARY(10) = 
( 
            SELECT MAX(lsn) 
            FROM   dbo.log_trg_cdc 
            WHERE  table_name = 'dbo.Fact_Battle_Result' 
); 


-- load process
WITH cte_incr_load AS 


( 
               SELECT   ROW_NUMBER() OVER ( PARTITION BY battleid ORDER BY __$start_lsn DESC, __$seqval DESC ) AS __$rn,
                        * 
               FROM     [cdc].[dbo_battle_CT] 
               WHERE    __$operation = 2 
               AND      __$start_lsn > @lsn 
) 
  
     , cte_full_load AS 
( 
              SELECT * 
              FROM   dbo.battle 
              WHERE  @lsn IS NULL ) 

	 , cte_union AS 
 ( 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     __$operation 
              FROM   cte_incr_load AS cte_l 
              WHERE  cte_l.__$rn = 1 
              UNION ALL 
              SELECT battleid, 
                     startdate, 
                     modetemplateid, 
                     tournamentid, 
                     2 AS __$operation 
              FROM   cte_full_load )
			  
MERGE fact_battle_result AS TARGET 
USING       ( 
                            SELECT     DD.datekey AS Date_Key, 
                                       DT.time_key, 
                                       DG.game_key, 
                                       cte.battleid, 
                                       DM.mode_key, 
                                       Win_Player_Key = MAX( 
                                       CASE 
                                                  WHEN result = 1 THEN DP.player_key 
                                                  ELSE -1 
                                       END), 
                                       Los_Player_Key = MAX( 
                                       CASE 
                                                  WHEN result = -1 THEN DP.player_key 
                                                  ELSE -1 
                                       END), 
                                       Is_Draw = MAX( 
                                       CASE 
                                                  WHEN result = 0 THEN 1 
                                                  ELSE 0 
                                       END), 
                                       cte.__$operation 
                            FROM       dbo.battlemembersxref AS BM 
                            INNER JOIN cte_union AS cte ON BM.battleid = cte.battleid 
                            INNER JOIN dbo.dim_player AS DP ON BM.playerid = DP.player_id 
                            INNER JOIN dbo.battle AS B ON BM.battleid = B.battleid 
                            INNER JOIN dbo.modetemplate AS M ON B.modetemplateid = M.modetemplateid 
                            INNER JOIN dbo.dim_mode AS DM ON M.modetemplateid = DM.mode_id 
                            INNER JOIN dbo.gametemplate AS G ON M.gametemplateid = G.gametemplateid 
                            INNER JOIN dbo.dim_game AS DG ON G.gametemplateid = DG.game_id 
                            INNER JOIN dbo.dim_date AS DD ON  CAST(B.startdate AS DATE) = DD.fulldate 
                            INNER JOIN dbo.dim_time AS DT ON  CONVERT(VARCHAR(8), B.startdate, 108) = DT.fulltimeext 
                            GROUP BY   cte.battleid, DM.mode_key, DG.game_key, DD.datekey, DT.time_key, cte.__$operation 
					) AS SOURCE 
ON TARGET.battle_key = SOURCE.battleid 
WHEN NOT matched BY TARGET THEN 
INSERT 
       ( 
              date_key, 
              time_key, 
              game_key, 
              battle_key, 
              mode_key, 
              win_player_key, 
              los_player_key, 
              is_draw 
       ) 
       VALUES 
       ( 
              date_key, 
              time_key, 
              game_key, 
              battleid, 
              mode_key, 
              win_player_key, 
              los_player_key, 
              is_draw 
       ); 

-- mark the end of the load process and the latest LSN
INSERT dbo.log_trg_cdc 
       ( 
              table_name, 
              lsn 
       ) 
       VALUES 
       ( 
              'dbo.Fact_Battle_Result', 
               ISNULL((SELECT MAX(__$start_lsn) FROM [cdc].[dbo_battle_ct]),0) 
       )
  END
