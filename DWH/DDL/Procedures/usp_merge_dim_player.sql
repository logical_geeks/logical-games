USE online_games; 

GO 

IF OBJECT_ID ('dbo.usp_merge_dim_player') IS NOT NULL 
  DROP PROCEDURE dbo.usp_merge_dim_player; 

GO 

CREATE PROCEDURE dbo.usp_merge_dim_player 
AS 
  BEGIN 
      MERGE dbo.dim_player AS TARGET 
      USING (SELECT playerid AS Player_ID, 
             LEFT(username, CHARINDEX('_', username) - 1) AS First_Name, 
             RIGHT(username, CHARINDEX('_', REVERSE(username)) - 1) AS Last_Name, 
             ISNULL(birthdate, '1901-01-01') AS Birthday, 
             ISNULL(LTRIM(RTRIM(city)), 'Unknown') AS city, 
             ISNULL(LTRIM(RTRIM(country)), 'Unknown') AS country, 
             ISNULL(LTRIM(RTRIM(about)), 'Unknown') AS Description, 
             ISNULL(registrationdate, '2010-01-01') AS Registration_Date 
             FROM dbo.player) AS SOURCE 
      ON ( TARGET.player_id = SOURCE.player_id ) 
      WHEN MATCHED AND TARGET.first_name <> SOURCE.first_name 
      OR TARGET.last_name <> SOURCE.last_name
      OR TARGET.birthday <> SOURCE.birthday 
      OR TARGET.city <> SOURCE.city
      OR TARGET.country <> SOURCE.country 
      OR TARGET.description <> SOURCE.description
      OR TARGET.registration_date <> SOURCE.registration_date 
      THEN 
        UPDATE SET TARGET.first_name = SOURCE.first_name, 
                   TARGET.last_name = SOURCE.last_name, 
                   TARGET.birthday = SOURCE.birthday, 
                   TARGET.city = SOURCE.city, 
                   TARGET.country = SOURCE.country, 
                   TARGET.description = SOURCE.description, 
                   TARGET.registration_date = SOURCE.registration_date 
      WHEN NOT matched BY target THEN 
        INSERT (player_id, 
                first_name, 
                last_name, 
                birthday, 
                country, 
                city, 
                description, 
                registration_date) 
        VALUES (player_id, 
                first_name, 
                last_name, 
                birthday, 
                country, 
                city, 
                description, 
                registration_date) 
      WHEN NOT MATCHED BY SOURCE THEN 
        DELETE; 
  END 
