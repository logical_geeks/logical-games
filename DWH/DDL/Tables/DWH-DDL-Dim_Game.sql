USE online_games; 

GO 

IF Object_id ('dbo.Dim_Game') IS NOT NULL 
  DROP TABLE dbo.Dim_Game; 

CREATE TABLE dbo.Dim_Game
  ( 
     game_key  INT NOT NULL IDENTITY, 
     game_id   INT NOT NULL, 
     game_name NVARCHAR(50) NOT NULL 
     CONSTRAINT pk_dim_game PRIMARY KEY (game_key) 
  ) ;