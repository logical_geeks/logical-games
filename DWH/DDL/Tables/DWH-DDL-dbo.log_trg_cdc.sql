USE online_games;
GO 
-- Enable CDC at the Database Level

EXEC sys.Sp_cdc_enable_db
GO 

-- Enable on the table level
EXEC sys.Sp_cdc_enable_table 
  @source_schema = N'dbo', 
  @source_name = N'battle', 
  @role_name = NULL, 
  @filegroup_name = N'Primary', 
  @supports_net_changes = 0
  GO 

-- Create a table to store the load log
IF OBJECT_ID('dbo.log_trg_cdc') IS NOT NULL DROP TABLE  dbo.log_trg_cdc;

GO

CREATE TABLE dbo.log_trg_cdc

             ( 
                          id_log     INT NOT NULL IDENTITY, 
                          table_name NVARCHAR(512) NOT NULL, 
                          dt         DATETIME NOT NULL DEFAULT GETDATE(), 
                          lsn        BINARY(10) NOT NULL DEFAULT(0x0), 
                          CONSTRAINT pk_log_cdc PRIMARY KEY (id_log) 
             )
GO 