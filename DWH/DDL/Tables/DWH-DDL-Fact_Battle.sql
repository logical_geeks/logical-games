USE online_games; 

GO 

IF OBJECT_ID ('dbo.Fact_Battle') IS NOT NULL 
  DROP TABLE dbo.Fact_Battle; 

GO

CREATE TABLE dbo.Fact_Battle 
  ( 
     fact_battle_id INT NOT NULL IDENTITY,
     battle_id INT NOT NULL,
     date_key INT NOT NULL, 
     time_key INT NOT NULL, 
     player_key INT NOT NULL,
     movedescription INT NOT NULL,
   
     CONSTRAINT pk_dboFact_Battle  PRIMARY KEY (fact_battle_id),
     CONSTRAINT Fact_Battle_Date_Key_Time_Key_Player_Key UNIQUE (date_key, time_key, player_key, movedescription)
  ) ;



