USE online_games; 

GO 

IF OBJECT_ID ('dbo.Dim_Date') IS NOT NULL 
  DROP TABLE dbo.Dim_Date; 

CREATE TABLE dbo.Dim_Date
  ( 
     date_key        INT NOT NULL, 
     fulldate        DATETIME NOT NULL, 
     dayofmonthvalue INT NOT NULL, 
     daynamevalue    NVARCHAR (10) NOT NULL, 
     weekofmonth     INT NOT NULL, 
     weekofquarter   INT NOT NULL, 
     weekofyear      INT NOT NULL, 
     monthnumber     INT NOT NULL, 
     monthnamevalue  NVARCHAR (15) NOT NULL, 
     monthofquarter  INT NOT NULL, 
     quartervalue    INT NOT NULL, 
     semester        TINYINT NOT NULL, 
     yearvalue       INT NOT NULL 
     CONSTRAINT pk_dim_date_datekey PRIMARY KEY (date_key) 
  ); 