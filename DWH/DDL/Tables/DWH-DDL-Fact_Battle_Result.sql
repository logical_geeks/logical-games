
IF OBJECT_ID ('dbo.Fact_Battle_Result') IS NOT NULL 
  DROP TABLE dbo.fact_battle_result; 

CREATE TABLE dbo.fact_battle_result 
  (  
     fact_battle_result_id INT NOT NULL IDENTITY,
     battle_id      INT NOT NULL,  
     date_key       INT NOT NULL, 
     time_key       INT NOT NULL, 
     game_key       INT NOT NULL, 
     mode_key       INT NOT NULL, 
     win_player_key INT NOT NULL, 
     los_player_key INT NOT NULL, 
     is_draw        BIT NOT NULL,
     duration       TIME NOT NULL

     CONSTRAINT pk_fact_battle_result PRIMARY KEY (fact_battle_result_id), 
     CONSTRAINT fact_battle_result_date_key_time_key_game_key_mode_key_win_player_key_los_player_key
     UNIQUE (date_key, time_key, game_key, mode_key, win_player_key, los_player_key) 
  ) ;
