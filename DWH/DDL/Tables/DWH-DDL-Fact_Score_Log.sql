USE online_games;

IF OBJECT_ID ('dbo.Fact_Score_Log') IS NOT NULL DROP TABLE dbo.Fact_Score_Log; 

GO

CREATE TABLE dbo.Fact_Score_Log (
	fact_score_log_id   INT   NOT NULL  IDENTITY,  
	battle_Id       INT   NOT NULL,
	date_Key       INT   NOT NULL, 
	time_Key       INT   NOT NULL, 
	player_Key     INT   NOT NULL,
	scored_points  INT   NOT NULL
	 
CONSTRAINT PK_Fact_Score_Log PRIMARY KEY (fact_score_log_id),
CONSTRAINT Fact_Score_Log_Date_Key_Time_Key_Player_Key UNIQUE(Date_Key,Time_Key, Player_Key));

	  