USE online_games; 

GO 

IF OBJECT_ID ('dbo.Dim_Mode') IS NOT NULL 
  DROP TABLE dbo.dim_mode; 

go 

CREATE TABLE dbo.dim_mode 
  ( 
     mode_key  INT NOT NULL IDENTITY, 
     mode_id   INT NOT NULL, 
     mode_name NVARCHAR(8) NOT NULL 
     CONSTRAINT pk_dim_mode PRIMARY KEY (mode_key) 
  ) ;