USE online_games; 

GO 

IF OBJECT_ID ('Dim_Time') IS NOT NULL 
  DROP TABLE dim_time; 

CREATE TABLE dbo.dim_time 
  ( 
     time_key      INT NOT NULL IDENTITY, 
     fulltime      INT NOT NULL, 
     fulltimeext   NVARCHAR(8) NOT NULL, 
     hournumber    TINYINT NOT NULL, 
     timeinseconds INT NOT NULL, 
     timegroupkey  INT NOT NULL, 
     timegroup     NVARCHAR(100) NOT NULL 
     CONSTRAINT pk_dim_time PRIMARY KEY (time_key) 
  ) ;